#!/bin/bash
set -e

terraform version
cp $TF_SECRET secret.tf
terraform init
