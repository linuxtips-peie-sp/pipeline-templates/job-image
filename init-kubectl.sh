#!/bin/bash
set -e

aws eks update-kubeconfig --name "$EKS_CLUSTER_NAME"
kubectl cluster-info
kubectl config get-contexts -o name
